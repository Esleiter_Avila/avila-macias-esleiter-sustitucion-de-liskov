using System;
using System.Collections.Generic;
using System.Text;

//Creo un objeto de la clase proceso perimetro la cual se llamará perimetro 
namespace Sustitucion_de_Liskov
{
    class ProcesoPerimetro:Calcular
    {
        public override double Proceso()
        {
            double result = (this.Base + this.Lado_A+this.Lado_B);
            return result;
        }
    }
}
