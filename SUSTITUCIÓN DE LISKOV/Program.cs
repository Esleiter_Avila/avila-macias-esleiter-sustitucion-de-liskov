using System;

namespace Sustitucion_de_Liskov
{
    class Program
    {
        static void Main(string[] args) 
        {
            //Creo un objeto de la clase triangulo la cual se llamará triangulo 1
            Triangulo triangulo1 = new Triangulo();

            //Creo un objeto de la clase proceso area llamada area 
            ProcesoArea area = new ProcesoArea();
            //Añadimos a este objeto sus atributos 
            area.Base = 20;
            area.Altura = 3;
            Console.WriteLine("El area es:");
            triangulo1.Proceso(area);

            ////Creo un objeto de la clase proceso perimetro la cual se llamará perimetro 
            ProcesoPerimetro perimetro = new ProcesoPerimetro();
            ////Añadimos a este objeto sus atributos 
            perimetro.Base = 8;
            perimetro.Lado_A = 10;
            perimetro.Lado_B = 15;
            Console.WriteLine("El perimetro es:");
            triangulo1.Proceso(perimetro);

        }
    }
}
