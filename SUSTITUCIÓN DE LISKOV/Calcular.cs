using System;
using System.Collections.Generic;
using System.Text;

//esta clase nos permitira saber el calculo
//de cada clase ya mencionanda en las anteriores clases

namespace Sustitucion_de_Liskov
{
    abstract class Calcular
    {
        public double Base { get; set; }
        public double Altura { get; set; }
        public double Lado_A { get; set; }
        public double Lado_B { get; set; }
        public abstract double Proceso();

    }
}
