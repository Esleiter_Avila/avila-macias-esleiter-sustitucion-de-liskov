using System;
using System.Collections.Generic;
using System.Text;

//Creo un objeto de la clase proceso area llamada area
//que nos permitira calcular la base por la altura
namespace Sustitucion_de_Liskov
{
    class ProcesoArea : Calcular
    {
        public override double Proceso()
        {
            double result = this.Base * this.Altura / 2;
            return result;
        }
    }
}
