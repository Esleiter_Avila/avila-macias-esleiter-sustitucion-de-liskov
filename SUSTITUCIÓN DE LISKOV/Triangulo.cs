using System;
using System.Collections.Generic;
using System.Text;

//Creo un objeto de la clase triangulo la cual se llamará triangulo 
namespace Sustitucion_de_Liskov
{
    class Triangulo 
    {
        public void Proceso(Calcular calculo)
        {
            double result = calculo.Proceso();
            Console.WriteLine("Resultado=" + result);
        }
    }
}
